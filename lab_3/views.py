from django.shortcuts import render
from datetime import datetime, date
from lab_1.models import Friend
from django.shortcuts import redirect, render
from .forms import friendForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    response = {'friends' : Friend.objects.all()}
    return render(request, 'lab3_index.html',response)


@login_required(login_url="/admin/login") 
def add_friend(request):
    context ={}
  
    # create object of form
    form = friendForm(request.POST or None)
      
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        return redirect('/lab-3')
    else:
        form = friendForm()
    
  
    context['form']= form
    return render(request, "lab3_form.html", context)
import 'package:flutter/material.dart';

class home extends StatelessWidget {
  const home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Home',
          style: TextStyle(color: Colors.black, fontFamily: 'MADE-Sunflower'),
        ),
      ),
      body: Center(
        child: Text(
          'No, this is not your home.',
          style: TextStyle(
              fontSize: 30, color: Colors.black, fontFamily: 'MADE-Sunflower'),
        ),
      ),
    );
  }
}

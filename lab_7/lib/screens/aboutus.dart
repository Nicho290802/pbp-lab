import 'package:flutter/material.dart';

class AboutUs extends StatelessWidget {
  const AboutUs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'About Us',
          style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.normal,
              fontFamily: 'MADE-Sunflower',
              fontSize: 30),
        ),
      ),
      body: Column(
        children: <Widget>[
          Container(
            padding: new EdgeInsets.fromLTRB(50, 250, 50, 0),
            child: Center(
              child: Text(
                'About Us',
                style: TextStyle(
                    fontSize: 50,
                    color: Colors.black,
                    fontFamily: 'MADE-Sunflower'),
              ),
            ),
          ),
          Container(
            padding: new EdgeInsets.fromLTRB(50, 0, 50, 50),
            child: Center(
              child: Text(
                'We are a team of students wanting a better workspace management system without all the confusing features. Inspired by the simpleness of using sticky notes for day-to-day reminders, we wanted to bring that to your screens by introducing an intuitive website that helps you keep track of your daily tasks.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

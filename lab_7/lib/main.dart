import 'package:flutter/material.dart';
import 'screens/aboutus.dart';
import './screens/home.dart';
import './screens/login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab 6 Flutter Nicholas ',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.brown,
        scaffoldBackgroundColor: Colors.orange[100],
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Center(
            child: Text('Stotes',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontFamily: 'MADE-Sunflower',
                    fontSize: 30))),
      ),
      body: SingleChildScrollView(
        child: Center(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: Column(
            // Column is also a layout widget. It takes a list of children and
            // arranges them vertically. By default, it sizes itself to fit its
            // children horizontally, and tries to be as tall as its parent.
            //
            // Invoke "debug painting" (press "p" in the console, choose the
            // "Toggle Debug Paint" action from the Flutter Inspector in Android
            // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
            // to see the wireframe for each widget.
            //
            // Column has various properties to control how it sizes itself and
            // how it positions its children. Here we use mainAxisAlignment to
            // center the children vertically; the main axis here is the vertical
            // axis because Columns are vertical (the cross axis would be
            // horizontal).
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(padding: new EdgeInsets.fromLTRB(50, 10, 50, 20)),
              Image.network(
                'https://very-awesome-projectmanager.herokuapp.com/static/img/task-view.png',
                width: 900,
                height: 300,
              ),
              Padding(padding: new EdgeInsets.fromLTRB(50, 10, 50, 10)),
              const Text(
                'Working from home has never been this easy.',
                style: TextStyle(fontSize: 30),
              ),
              Text(
                'Feel the benefits and be more productive with us.',
                style: TextStyle(fontSize: 30),
              ),
              Padding(padding: new EdgeInsets.fromLTRB(50, 10, 50, 10)),
              Text(
                'Register Now!',
                style: TextStyle(fontSize: 20),
              ),
              Padding(padding: new EdgeInsets.fromLTRB(50, 10, 50, 10)),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => BelajarForm()),
                  );
                },
                child: const Text('Register'),
              ),
              Padding(padding: new EdgeInsets.fromLTRB(50, 50, 50, 20)),
              Image.network(
                'https://very-awesome-projectmanager.herokuapp.com/static/img/meeting-list.png',
                width: 900,
                height: 300,
              ),
              Padding(padding: new EdgeInsets.fromLTRB(50, 10, 50, 20)),
              const Text(
                'Meetings',
                style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'MADE-Sunflower'),
              ),
              Text(
                'Organize your meetings with us.',
                style: TextStyle(fontSize: 20),
              ),
              Padding(padding: new EdgeInsets.fromLTRB(50, 50, 50, 20)),
              Image.network(
                'https://very-awesome-projectmanager.herokuapp.com/static/img/projects.png',
                width: 900,
                height: 300,
              ),
              Padding(padding: new EdgeInsets.fromLTRB(50, 10, 50, 20)),
              const Text(
                'Projects',
                style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'MADE-Sunflower'),
              ),
              Text(
                'Add, organize, and finish your projects all in one platform.',
                style: TextStyle(fontSize: 20),
              ),
              Padding(padding: new EdgeInsets.fromLTRB(50, 10, 50, 20)),
            ],
          ),
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.brown,
              ),
              child: Text(
                'Menu',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 30,
                    fontFamily: 'MADE-Sunflower'),
              ),
            ),

            // for item 1
            ListTile(
              title: Text('Home'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => home(),
                  ),
                );
              },
            ),

            // for item 2

            ListTile(
              title: Text('About Us'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AboutUs(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

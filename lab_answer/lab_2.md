1. Difference between JSON and XML?
JSON is a file format for storing and sending data objects with attribute-value pairs and arrays that employs human-readable text. JSON is a standard for storing data that is well-organized and accessible. JavaScript Object Notation (JSON) is an acronym for JavaScript Object Notation. It provides a logically accessible collection of data that is human-readable.

XML is a data-storage-oriented extensible markup language. It is commonly used for data transport. There is a case difference. You can use XML to define markup elements and create your own markup language. In the XML language, an element is a fundamental unit. The.xml file extension is used for XML files.

2. Difference between HTML and XML?
eXtensible Markup Language is the abbreviation for eXtensible Markup Language. It's a basic markup language that's simply used to convey data, not to display it. It supports Unicode textual data formats to accommodate a variety of human languages. The basic goal of XML is to make the internet more simple and usable. It's most commonly used to represent sophisticated data structures like those seen in online services. The.xml extension appears at the conclusion of every XML document.

HyperText Markup Language (HTML) stands for HyperText Markup Language. HTML, unlike an XML document, is used to create and edit web pages. To put it another way, HTML deals with the various ways in which data can be displayed on the browser. HTML is case-insensitive and may be updated with any basic text editor, such as Notepad for Windows or TextEdit for iOS. HTML uses predefined tags and attributes to function. We may develop many types of designs and structures for web pages using these predefined tags, which can then be presented to users via a web browser. It's time to compare and contrast XML and HTML in depth.

Bibliography :
1. https://www.guru99.com/json-vs-xml-difference.html
2. https://www.javatpoint.com/html-vs-xml
